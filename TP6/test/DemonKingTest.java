package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import adventurer.Adventurer;
import adventurer.Mage;
import adventurer.Rogue;
import adventurer.Warrior;
import enemy.DemonKing;
import item.Artifact;
import item.Weapon;
import team.Team;

public class DemonKingTest {

    Adventurer adventurer = new Mage("Lance", 200, 300);
    DemonKing demonKing  = new DemonKing();

    // ref: https://stackoverflow.com/questions/1119385/junit-test-for-system-out-println
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void attackTest(){
        demonKing.attack(adventurer);
        assertEquals(outContent.toString().trim(),"The demon attacked Lance! their HP has been decreased to 100\n".trim());
    }

    public Team setupLosingTeam(){
        Team team = new Team("Stardrop Slasher");
        team.addAdventurer(new Mage("Lance", 200, 300));
        team.addAdventurer(new Warrior("Louie", 110, 210));
        team.addAdventurer(new Rogue("Luna", 200, 120));
        team.addArtifact(new Artifact());
        team.addWeapon(new Weapon());

        return team;
    }

    public Team setupWinningTeam(){
        Team team = new Team("Crimson Castle");
        team.addAdventurer(new Mage("Camilla", 250, 250));
        team.addAdventurer(new Rogue("Isaac", 110, 350));
        team.addArtifact(new Artifact());
        team.addWeapon(new Weapon());

        return team;
    }

    @Test 
    public void fightLoseTest(){
        Boolean result =  demonKing.fight(setupLosingTeam());
        assertEquals(false, result);
    }
    
    @Test
    public void fightWinTest(){
        Boolean result= demonKing.fight(setupWinningTeam());
        assertEquals(true, result);
    }

    @Test
    public void fightEmptyTest(){
        demonKing.fight(new Team("Empty Team"));
        assertEquals(outContent.toString().trim(),"The team is empty!\n".trim());
    }

}
