import java.util.ArrayList;
import java.util.List;
import item.Potion;
import item.Artifact;
import item.Weapon;
public class Team {
    private String ID;
    private String name;
    private List<Adventurer> adventurerList;
    private Backpack itemBackpack;
    private static int teamCount;

    public Team(String name) {
        this.name = name;
        this.ID = Integer.toString(teamCount);
        teamCount++;
        this.adventurerList = new ArrayList<>();
        this.itemBackpack = new Backpack<>();
    }

    // TODO: Implemen metode-metode yang berkaitan dengan penambahan item
    public String addArtifact(Artifact artifact){
        itemBackpack.add(artifact);
        return "Artifact has been added succesfully";
    }
    public String addPotion(Potion potion){
        itemBackpack.add(potion);
        return "Potion has been added succesfully";
    }
    public String addWeapon(Weapon weapon){
        itemBackpack.add(weapon);
        return "Weapon has been added succesfully";
    }

    public String addAdventurer(Adventurer adventurer) {
        if (adventurerList.contains(adventurer)){
            return adventurer.name + " is already a member of this team!";
        }
        adventurerList.add(adventurer);

        return "Team member added successfully.";
    }

    // setter getter

    public Backpack getBackpack(){
        return itemBackpack;
    }
    
    public String getID() {
        return ID;
    }

    public void setID(String iD) {
        ID = iD;
    }

    public List<Adventurer> getAdventurerList() {
        return adventurerList;
    }

    public void setAdventurerList(List<Adventurer> adventurerList) {
        this.adventurerList = adventurerList;
    }

    public String getName(){
        return name;
    }



}
