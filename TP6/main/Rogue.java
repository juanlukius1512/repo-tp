
import enemy.DemonKing;

public class Rogue extends Adventurer {


    public Rogue(String name, int HP, int ATK) {
        super(name, HP, ATK);
    }

    // TODO: Implemen attack(), kurangin hp demonking
    @Override
    public String attack(DemonKing demonKing) {
        demonKing.setHP(demonKing.getHP() - ATK);
        return "The demon attacked ! their HP has been decreased to " + demonKing.getHP();
    }
    
}
