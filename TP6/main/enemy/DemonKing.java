package enemy;

import java.util.List;
import adventurer.Adventurer;
import item.Artifact;
import item.Backpack;
import item.Potion;
import item.Weapon;
import item.ability.Enhancer;
import item.ability.Healer;
import team.Team;

public class DemonKing {
    private int demonHP = 1000;
    private int demonATK = 100;

    public void attack(Adventurer adventurer){
        adventurer.setHP(adventurer.getHP() - demonATK);
        System.out.println("The demon attacked " + adventurer.name +"! their HP has been decreased to " +adventurer.getHP());
    }

    public void setHP(int amount){
        this.demonHP = amount;
    }

    public int getHP(){
        return demonHP;
    }

    public Boolean fight(Team team){

        boolean demonDefeated = false;
        boolean adventurerDefeated = false;

        List<Adventurer> adventurers = team.getAdventurerList();
        Backpack itemBackpack = team.getBackpack();

        boolean battle = true;
        int pointer = adventurers.size() - 1;
        if(pointer < 0){
            System.out.println("The team is empty!");
            return demonDefeated;
        }

        Adventurer adventurer = adventurers.get(pointer);
        int initialHP = adventurer.HP;

        System.out.println("- - - - - - - BATTLE START - - - - - - - ");

        while (battle) {

            // Demon Attack Adventurer
            this.attack(adventurer);

            if(adventurer.getHP() <= 0){ // passed out
                adventurerDefeated = true;

            } else {

                // Adventurer Use Item
                Object item = itemBackpack.get(0);
                adventurer.useItem(item);

                // Adventurer attack demon
                System.out.println(adventurer.attack(this));

                if(demonHP <= 0){
                    System.out.println("\nCongratulations!! The demon king has finally been defeated!\n");
                    demonDefeated = true;
                    break;
                }

            }

            if(adventurerDefeated){
                adventurer.setHP(initialHP); // Reset condition
                System.out.println(adventurer.name + " has passed out!\n");
                adventurerDefeated = false;

                pointer -= 1;
                if(pointer < 0){
                    System.out.println("The team has lost! The demon king still is standing strong with his remaining " + demonHP +" HP\n");
                    demonDefeated = false;
                    break;

                } else{
                    adventurer = adventurers.get(pointer);
                    initialHP = adventurer.HP;
                }
            }
            
        }

        System.out.println("- - - - - - - BATTLE END - - - - - - - ");

        return demonDefeated;
    }
}
