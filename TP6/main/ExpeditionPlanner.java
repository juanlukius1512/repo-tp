import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import enemy.DemonKing;
/*
 *  System to simulate the expedition to defeat The Demon King
 *  It predicts the outcome of the fight, depending on each member's abilities and supplies.
 */
public class ExpeditionPlanner {

        
    private List<Adventurer> adventurerList;
    private List<Team> teamList;

    public ExpeditionPlanner(){
        adventurerList = new ArrayList<>();
        teamList = new ArrayList<>();
    }

    public static void main(String[]args){
        ExpeditionPlanner XP = new ExpeditionPlanner();
        XP.run();
    }

    public void run(){
        Scanner scanner = new Scanner(System.in);
        boolean running = true;

        
        while (running) {
            displayMenu();
            int choice = scanner.nextInt();
            scanner.nextLine(); // Consume newline

            switch (choice) {
                case 1:
                    addTeam(scanner);
                    break;
                case 2:
                    addAdventurer(scanner);
                    break;
                case 3:
                    addTeamMember(scanner);
                    break;
                case 4:
                    displayTeams();
                    break;
                case 5:
                    displayAdventurers();
                    break;
                case 6:
                    doExpedition(scanner);
                    break;
                case 7:
                    addExpeditionSupply(scanner);
                    break;
                case 8:
                    running = false;
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        }
        scanner.close();
        System.out.println("Thank you for using eXpedition Planner!");
    }

    public void addTeam(Scanner scanner){
        System.out.print("Enter team name: ");
        String name = scanner.nextLine();
        Team newTeam = new Team(name);
        teamList.add(newTeam);
        System.out.println("Team added successfully.");
    }

    
    public void addAdventurer(Scanner scanner) {
        System.out.print("Enter adventurer type (Warrior/Mage/Rogue): ");
        String type = scanner.nextLine();
        System.out.print("Enter name: ");
        String name = scanner.nextLine();
        System.out.print("Enter HP: ");
        int hp = scanner.nextInt();
        System.out.print("Enter ATK: ");
        int atk = scanner.nextInt();
        scanner.nextLine(); // Consume newline

        addAdventurer(type, name, hp, atk);
        System.out.println("Adventurer added successfully.");
    }

    public Adventurer addAdventurer(String type, String name, int HP, int ATK) {
        Adventurer newAdventurer;
        switch (type.toLowerCase()) {
            case "warrior":
                newAdventurer = new Warrior(name, HP, ATK);
                break;
            case "mage":
                newAdventurer = new Mage(name, HP, ATK);
                break;
            case "rogue":
                newAdventurer = new Rogue(name, HP, ATK);
                break;
            default:
                System.out.println("Invalid adventurer type");
                return null;
        }
        adventurerList.add(newAdventurer);

        return newAdventurer;
    }

    public void addTeamMember(Scanner scanner) {
        System.out.println("Available Team:");
        for (Team team : teamList){
            System.out.println("Team: " + team.getName() + " (ID: " + team.getID() + ")");
        }
        System.out.print("Enter team ID: ");
        String teamID = scanner.nextLine();
        System.out.println("Available Adventurer:");
        for (Adventurer adventurer : adventurerList){
            System.out.println("Adventurer: " + adventurer.name + " (ID: " + adventurer.ID + ")");
        }
        System.out.print("Enter adventurer ID: ");
        String adventurerID = scanner.nextLine();

        Team team = findTeamByID(teamID);
        Adventurer adventurer = findAdventurerByID(adventurerID);
        if (team != null && adventurer != null) {
            System.out.println(team.addAdventurer(adventurer));
        } else {
            System.out.println("Team or adventurer not found.");
        }
    }

    private Team findTeamByID(String ID) {
        return teamList.stream()
                .filter(team -> team.getID().equals(ID))
                .findFirst()
                .orElse(null);
    }

    private Adventurer findAdventurerByID(String ID) {
        return adventurerList.stream()
                .filter(adv -> adv.ID.equals(ID))
                .findFirst()
                .orElse(null);
    }

    public void displayTeams() {
        for (Team team : teamList) {
            System.out.println("Team ID: " + team.getID());
            System.out.println("Team Name: " + team.getName());
            System.out.println("Members: " + team.getAdventurerList().size());
            System.out.println("Supplies: " + team.getBackpack().toString());
            System.out.println("--------------------");
        }
    }

    public void displayAdventurers() {
        for (Adventurer adv : adventurerList) {
            System.out.println("Adventurer: " + adv.name + " (ID: " + adv.ID + ")");
            System.out.println("Type: " + adv.getClass().getSimpleName());
            System.out.println("HP: " + adv.HP + ", ATK: " + adv.ATK);
            System.out.println("--------------------");
        }
    }

    public void doExpedition(Scanner scanner){
        System.out.println("Available Team:");
        for (Team team : teamList){
            System.out.println("Team: " + team.getName() + " (ID: " + team.getID() + ")");
        }
        System.out.print("Enter team ID: ");
        String teamID = scanner.nextLine();
        Team team = findTeamByID(teamID);

        if(team == null){
            System.out.println("Can not find the team.");
        } else {
            startExpedition(team);
        }
    }

    // TODO: Lengkapi fitur add supply, jangan lupa terapkan defensive programming.
    public void addExpeditionSupply(Scanner scanner){
        if scanner = "Artifact"{
            .addArtifact(scanner);
        }
        else if scanner = "Potion"{
            team.addPotion(scanner);
        }

    }

    // TODO: Implemen method yang berkaitan dengan addItem
    public void addItem(String type, Team team){
    }


    private void displayMenu() {
        System.out.println("\n========== eXpedition Planner ==========");
        System.out.println("1. Add Team");
        System.out.println("2. Add Adventurer");
        System.out.println("3. Add Team Member");
        System.out.println("4. Display Teams");
        System.out.println("5. Display Adventurers");
        System.out.println("6. Start Expedition");
        System.out.println("7. Add Expedition Supply");
        System.out.println("8. Exit");
        System.out.print("Enter your choice: ");
    }

    public static void startExpedition(Team team){
        DemonKing demonKing = new DemonKing();
        demonKing.fight(team);
    }
}
