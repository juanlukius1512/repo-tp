package item;
// TODO: Implemen Interface Healer dan Enhancer

import ability.enhancer;
import ability.healer;

public class Artifact implements enhancer, healer{
    public String increaseAttack(){
        return "An Artifact is used for ENHANCING attack! (+50 ATK)";
    }
    @Override
    public String toString() {
        return "Artifact";
    }
    
}
