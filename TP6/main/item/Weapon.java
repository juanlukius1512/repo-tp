package item;
import ability.enhancer;
public class Weapon implements enhancer{

    @Override 
    public String increaseAttack(){
        return "A Weapon is used for ENHANCING attack! (+50 ATK)";
    }

    @Override
    public String toString() {
        return "Weapon";
    }
    
}
