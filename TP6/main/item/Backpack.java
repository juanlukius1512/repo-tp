package item;
import java.util.ArrayList;
// TODO: Implemen Generic class Backpack
public class Backpack<T> {

    private ArrayList<T> itemList =  new ArrayList<>();
    private int used;

    public void add(T item){
        itemList.add(item);
    }

    public T get(int i){
        if (i < 0 || i >= itemList.size()) {
            throw new IndexOutOfBoundsException("Index " + i + " is out of bounds.");
        }
        used++;
        return itemList.remove(i);
    }

    public String getInformation(){
       // TODO: Implementasi sesuai format dokumen
       return null;
    }

    @Override
    public String toString(){
        // TODO: Implementasi sesuai format dokumen, digunakan dalam displayTeam\
        return null;
    }

}
