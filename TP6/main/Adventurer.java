import ability.enhancer;
import ability.healer;
import enemy.DemonKing;

public abstract class Adventurer {
    public String name;
    public String ID;
    public int HP;
    public int ATK;
    public int MaxHP;
    protected static int advCount;

    public Adventurer(String name, int HP, int ATK) {
        this.name = name;
        this.HP = HP;
        this.ATK = ATK;
        this.ID = Integer.toString(advCount);
        advCount++;
        this.MaxHP = HP;
    }

    // TODO: Implemen attack(), kurangi hp demonking berdasarkan ATK
    public abstract String attack(DemonKing demonKing);

    // TODO: Implemen UseItem
    public void useItem(Object item){
        if (item instanceof healer){
            ATK += 50;
        }
        else if (item instanceof enhancer){
            HP += 50;
            if (HP > MaxHP){
                HP = MaxHP;
            }
        }
    }


    // setter getter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int hP) {
        HP = hP;
    }

    public int getATK() {
        return ATK;
    }

    public void setATK(int aTK) {
        ATK = aTK;
    }
    public int getMaxHP(){
        return MaxHP;
    }
    
}
