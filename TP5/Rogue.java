class Rogue extends Adventurer {
    public Rogue(String name, int HP, int DEF, int ATK, int MATK) {
        super(name,HP,DEF,ATK,MATK);
    }

    @Override
    protected String getIDPrefix() {
        return "003";
    }

    @Override
    protected String getSubClass() {
        return "Rogue";
    }
}