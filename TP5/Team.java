import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class Team {
    private String ID;
    private List<Adventurer> adventurerList;
    private int totalHP;
    private int totalDEF;
    private int totalATK;
    private int totalMATK;
    private String specialAbility;
    private static int teamCount;
    private int member;

    public Team() {
        this.ID = "0" + teamCount++;
        this.adventurerList = new ArrayList<>(); 
    }

    public String addAdventurer(Adventurer adventurer) {
    for (Adventurer adventurers : adventurerList){
        if (adventurers.getID().equals(adventurer.getID())){
            return "Adventurer is already in team";
        }
    }
    adventurerList.add(adventurer);
    updateStats();
    return "Adventurer has been added succesfully";
}

    private void updateStats() {
        for (Adventurer adventurer : adventurerList){
            totalHP += adventurer.getHP();
            totalATK += adventurer.getATK();
            totalDEF += adventurer.getDEF();
            totalMATK += adventurer.getMATK();
            member += 1;
        }
    }

    private void updateSpecialAbility() {
        Set<String> subclassesSet = adventurerList.stream()
                .map(Adventurer::getSubClass)
                .collect(Collectors.toSet());

        String ability;
        if (subclassesSet.equals(Set.of("Mage"))) {
            ability = "Arcane Precision - Increased magic damage";
        } else if (subclassesSet.equals(Set.of("Mage", "Rogue"))) {
            ability = "Arcane Trickery - Chance to apply crowd control effects";
        } else if (subclassesSet.equals(Set.of("Mage", "Warrior"))) {
            ability = "Spellblade Synergy - Warrior gains magic damage, Mage gains physical defense";
        } else if (subclassesSet.equals(Set.of("Mage", "Warrior", "Rogue"))) {
            ability = "Perfect Balance - All stats slightly increased";
        } else if (subclassesSet.equals(Set.of("Rogue"))) {
            ability = "Lone Wolf - Increased critical hit chance";
        } else if (subclassesSet.equals(Set.of("Rogue", "Warrior"))) {
            ability = "Precision Strike - Increased damage against armored targets";
        } else if (subclassesSet.equals(Set.of("Warrior"))) {
            ability = "Solo Tankiness - Increased defense";
        } else {
            ability = "No special ability for this combination";
        }
        specialAbility = ability;
    }



     public String getID(){
        return  ID;
     }
     public int getHP(){
        return totalHP;
}    
    public int getDEF(){
        return totalDEF;
    }
    public int getATK(){
        return totalATK;
    }
    public int getMATK(){
        return totalMATK;
    }
    public int getTeamCount(){
        return teamCount;
    }
    public String getSpecialAbility(){
        return specialAbility;
    }
    public int GetMemberCount(){
        return member;

}
}