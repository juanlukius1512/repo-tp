import java.util.*;

public class GuildMaster {
    private List<Adventurer> adventurerList;
    private List<Team> teamList;
    private static final String ADVENTURER_FILE = "adventurers.csv";
    private static final String TEAM_FILE_PREFIX = "team_";
    private static final String TEAM_FILE_SUFFIX = ".txt";

    public GuildMaster() {
        adventurerList = new ArrayList<>();
        teamList = new ArrayList<>();
    }

    public void run() {
        load();
        Scanner scanner = new Scanner(System.in);
        boolean running = true;

        while (running) {
            displayMenu();
            int choice = scanner.nextInt();
            scanner.nextLine(); // Consume newline

            switch (choice) {
                case 1:
                    addTeam(scanner);
                    break;
                case 2:
                    addAdventurer(scanner);
                    break;
                case 3:
                    addTeamMember(scanner);
                    break;
                case 4:
                    displayTeams();
                    break;
                case 5:
                    displayAdventurers();
                    break;
                case 6:
                    running = false;
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        }
        scanner.close();
        System.out.println("Thank you for using the Guild Management System!");
    }

    private void displayMenu() {
        System.out.println("\n--- Guild Management System ---");
        System.out.println("1. Add Team");
        System.out.println("2. Add Adventurer");
        System.out.println("3. Add Team Member");
        System.out.println("4. Display Teams");
        System.out.println("5. Display Adventurers");
        System.out.println("6. Exit");
        System.out.print("Enter your choice: ");
    }

    public void load() {
        loadAdventurers();
        loadTeams();
    }

    private void loadAdventurers() {
        // TODO
    }

    private void loadTeams() {
        // TODO
    }

    public void addTeam(Scanner scanner) {
        Team newTeam = new Team();
        teamList.add(newTeam);
        saveTeam(newTeam);
        System.out.println("Team added successfully.");
    }

    public void addAdventurer(Scanner scanner) {
        System.out.print("Enter adventurer type (Warrior/Mage/Rogue): ");
        String type = scanner.nextLine();
        System.out.print("Enter name: ");
        String name = scanner.nextLine();
        System.out.print("Enter HP: ");
        int hp = scanner.nextInt();
        System.out.print("Enter DEF: ");
        int def = scanner.nextInt();
        System.out.print("Enter ATK: ");
        int atk = scanner.nextInt();
        System.out.print("Enter MATK: ");
        int matk = scanner.nextInt();
        scanner.nextLine(); // Consume newline
        Adventurer adventurer = addAdventurer(type, name, hp, def, atk, matk);
        if (adventurer == null) {
            System.out.println("Invalid Adventurer Type.");
        } else {
        System.out.println("Adventurer added successfully.");
        adventurerList.add(adventurer);
    }
}

    public Adventurer addAdventurer(String type, String name, int HP, int DEF, int ATK, int MATK) {
        Adventurer adventurer;
    if (type.equals("Warrior")){
        adventurer = new Warrior(name,HP,DEF,ATK,MATK);
    }
    else if (type.equals("Mage")){
        adventurer = new Mage(name,HP,DEF,ATK,MATK);
    }
    else if (type.equals("Rogue")){
        adventurer = new Rogue(name,HP,DEF,ATK,MATK);
    }
    else {
        return null;
    }
    return adventurer;
    }

    public void addTeamMember(Scanner scanner) {
        System.out.println("Available Team:");
        for (Team team : teamList){
            System.out.println(team.getID());
        }
        System.out.print("Enter team ID: ");
        String TeamID = scanner.nextLine();
        Team selectedteam = findTeamByID(TeamID);
        System.out.println("Available Adventurer:");
        for (Adventurer adventurer : adventurerList){
            System.out.println(adventurer.getID() + "-" + adventurer.getName());
        }
        System.out.print("Enter adventurer ID: ");
        String selectedadventurerID = scanner.nextLine();
        Adventurer selectedadventurer = findAdventurerByID(selectedadventurerID);
        selectedteam.addAdventurer(selectedadventurer);
    }



    public void displayTeams() {
        for (Team team : teamList){
        System.out.println("Team ID : " + team.getID());
        System.out.println("Member : " + team.GetMemberCount());
        System.out.println("Total HP : " + team.getHP());
        System.out.println("Total DEF : " + team.getDEF());
        System.out.println("Total MATK : "+ team.getMATK());
        System.out.println("Special Ability : " + team.getSpecialAbility());
    }
    }

    public void displayAdventurers() {
            for (Adventurer adventurer : adventurerList){
                System.out.println("Adventurer " + adventurer.getName() + " ( ID : " + adventurer.getID() + " )");
                System.out.println("Type : " + adventurer.getSubClass());
                System.out.println("HP : " + adventurer.getHP() + ", DEF : " + adventurer.getDEF() + ", ATK : " + adventurer.getATK() + ", MATK : " + adventurer.getMATK());        }
    }
    

    private Team findTeamByID(String ID) {
        return teamList.stream()
                .filter(team -> team.getID().equals(ID))
                .findFirst()
                .orElse(null);
    }

    private Adventurer findAdventurerByID(String ID) {
        return adventurerList.stream()
                .filter(adv -> adv.ID.equals(ID))
                .findFirst()
                .orElse(null);
    }

    private void saveAdventurers() {
        // TODO
    }

    private void saveTeam(Team team) {
        // TODO
    }

    public static void main(String[] args) {
        GuildMaster guildmaster = new GuildMaster();
        guildmaster.run();
    }
}
    