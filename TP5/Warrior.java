class Warrior extends Adventurer {
    public Warrior(String name, int HP, int DEF, int ATK, int MATK) {
        super(name,HP,DEF,ATK,MATK);
    }

    @Override
    protected String getIDPrefix() {
        return "001";
    }

    @Override
    protected String getSubClass() {
        return "Warrior";
    }
  
}
