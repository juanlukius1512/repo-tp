abstract class Adventurer {
    protected String name;
    protected String ID;
    protected int HP;
    protected int DEF;
    protected int ATK;
    protected int MATK;
    protected static int advCount;

    public Adventurer(String name, int HP, int DEF, int ATK, int MATK) {
        this.name = name;
        this.HP = HP;
        this.DEF = DEF;
        this.ATK = ATK;
        this.MATK = MATK;
        this.ID = getIDPrefix() + advCount++;
    }

    protected abstract String getIDPrefix();
    protected abstract String getSubClass();
    
    public String toString(){
        return String.format("%s - %s", this.ID, this.name);
    }
    public String getName(){
        return name;
}
    public int getHP(){
        return HP;
}    
    public int getDEF(){
        return DEF;
    }
    public int getATK(){
        return ATK;
    }
    public int getMATK(){
        return MATK;
    }
    public String getID(){
        return ID;
}
}