class Mage extends Adventurer {
    public Mage(String name, int HP, int DEF, int ATK, int MATK) {
     super(name,HP,DEF,ATK,MATK);
    }

    @Override
    protected String getIDPrefix() {
        return "002";
    }

    @Override
    protected String getSubClass() {
        return "Mage";
    }
    

}
