import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GuildMaster {
    private List<Mission> missionList;
    private List<Adventurer> adventurerList;
    private List<Kingdom> kingdomList;
    private Scanner scanner;

    public GuildMaster() {
        missionList = new ArrayList<>();
        adventurerList = new ArrayList<>();
        kingdomList = new ArrayList<>();
        scanner = new Scanner(System.in);
        initializeKingdoms();
    }

    private void initializeKingdoms() {
        kingdomList.add(new WaterKingdom());
        kingdomList.add(new FireKingdom());
        kingdomList.add(new AirKingdom());
        kingdomList.add(new EarthKingdom());
    }

    public void run() {
        boolean running = true;
        while (running) {
            displayMainMenu();
            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    addMission();
                    break;
                case 2:
                    addMissionMember();
                    break;
                case 3:
                    removeMissionMember();
                    break;
                case 4:
                    addAdventurer();
                    break;
                case 5:
                    displayMissionDetails();
                    break;
                case 6:
                    displayKingdomDetails();
                    break;
                case 7:
                    listMissions();
                    break;
                case 8:
                    running = false;
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        }
    }

    private void displayMainMenu() {
        System.out.println("\nGuildMaster Menu:");
        System.out.println("1. Add Mission");
        System.out.println("2. Add Mission Member");
        System.out.println("3. Remove Mission Member");
        System.out.println("4. Add Adventurer");
        System.out.println("5. Display Mission Details");
        System.out.println("6. Display Kingdom Details");
        System.out.println("7. List Missions");
        System.out.println("8. Exit");
        System.out.print("Enter your choice: ");
    }

    private void addMission() {
        // TODO
    }

    private void addMissionMember() {
        // TODO
    }

    private void removeMissionMember() {
        // TODO
    }

    private void addAdventurer() {
        // TODO
    }

    private void displayMissionDetails() {
        // TODO
    }

    private void displayKingdomDetails() {
        // TODO
    }

    private void listMissions() {
        // TODO
    }

    // Helper method untuk menemukan adventurer berdasarkan nama
    private Adventurer findAdventurer(String name) {
        return adventurerList.stream()
                .filter(a -> a.getName().equalsIgnoreCase(name))
                .findFirst()
                .orElse(null);
    }

    public static void main(String[] args) {
        GuildMaster guildMaster = new GuildMaster();
        guildMaster.run();
    }
}