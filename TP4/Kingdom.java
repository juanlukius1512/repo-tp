
import java.util.ArrayList;
import java.util.List;

public abstract class Kingdom {
    protected String status;
    protected List<Adventurer> adventurerList;

    // TODO Demo
    public Kingdom() {
        this.status = status;
        this.adventurerList = new ArrayList<>();
        
    }

    public abstract void addAdventurer(Adventurer adventurer);
    // TODO tambahkan abstract method addAdventurer

    public String getReport() {
        return this.getClass().getSimpleName() + " is " + status;
    }

    public int getStatusWeight() {
        switch (status) {
            case "Disastrous": return 1;
            case "Neutral": return 2;
            case "Prosperous": return 3;
            default: return 0;
        }
    }
    public abstract String getName();
    public String getDetailedInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName()).append(" has ").append(adventurerList.size()).append(" adventurers");
        long masterCount = adventurerList.stream().filter(a -> a instanceof Master).count();
        sb.append(" and ").append(masterCount).append(" masters.\n");

        if (adventurerList.isEmpty()) {
            sb.append("There is no adventurer in ").append(this.getClass().getSimpleName()).append(".\n");
        } else {
            for (int i = 0; i < adventurerList.size(); i++) {
                Adventurer adventurer = adventurerList.get(i);
                sb.append(i + 1).append(". ").append(adventurer.getName()).append(" - ")
                  .append(adventurer.calculateSalary()).append(" - ")
                  .append(adventurer.getMissionsString()).append("\n");
            }
        }
        return sb.toString();
    }

}