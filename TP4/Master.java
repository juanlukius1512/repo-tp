
// TODO
public class Master extends Adventurer {
    public Master(String name, String rating, double salaryMultiplier, Kingdom kingdom) {
        super(name, rating, salaryMultiplier, kingdom);
    }

    @Override
    public int calculateSalary() {
        return (getRatingWeight() * 25) + (kingdom.getStatusWeight() * 100);
    }

    private int getRatingWeight() {
        switch (rating) {
            case "S": return 4;
            case "A": return 3;
            case "B": return 2;
            case "C": return 1;
            default: return 0;
        }
    }

    @Override
    public void addMission(Mission mission) {
        // TODO
    }

    @Override
    public int getMaxMissions() {
        return 3;
    }
}