
import java.util.ArrayList;
import java.util.List;

public class Mission {
    private String name;
    private String date;
    private Adventurer missionLeader;
    private List<Adventurer> memberList;

    public Mission(String name, String date) {
        this.name = name;
        this.date = date;
        this.memberList = new ArrayList<>();
    }

    public void addMember(Adventurer adventurer) {
        if adventurer.getMissionsString()
        if (adventurer instanceof Master && missionLeader == null ){
            missionLeader = adventurer;
        }
        else {
        memberList.add(adventurer);
    }
}

    public void removeMember(Adventurer adventurer) {
        memberList.remove(adventurer);
    }

    @Override
    public String toString() {
        // TODO
        return null
    }

    public String getName() {
        return name;
    }

    public boolean hasMember(Adventurer adventurer) {
        if (memberList.isEmpty()){
        return false;
        }
        return true;
    }

    public boolean hasMentor() {
        for (int i = 0; i < memberList.size(); i++){
        if (memberList.get(i) instanceof Mentor || missionLeader instanceof Mentor){
            return true;
        }
    }
    return false;
}

    public void displayMemberList() {
        for (int i = 0; i < memberList.size(); i++){
            System.out.println(memberList.get(i).getName());
        }
    }

    public String getDetailedInfo() {
        String str = name;
        str += "Mission Details\n";
        if (missionLeader == null ){
            str += "Leader : none\n";
        }
        else {
            str += missionLeader.getName() + "-" + missionLeader.getKingdomName();
        }
        if (memberList.isEmpty()){
            str += "Member : none\n";
        } 
        else {
        for (int i = 0; i < memberList.size(); i++){
        str += memberList.get(i).getName() + "-" + memberList.get(i).getKingdomName();
        }
    }
    return str;

    }

    // Getters
    public List<Adventurer> getMemberList() {
        return memberList;
    }

    public Adventurer getMissionLeader() {
        return missionLeader;
    }
    public String getDate(){
        return date;
    } 



}