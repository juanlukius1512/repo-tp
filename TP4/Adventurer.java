import java.util.ArrayList;
import java.util.List;

public abstract class Adventurer {
    protected String name;
    protected String rating;
    protected double salaryMultiplier;
    protected Kingdom kingdom;
    protected List<Mission> missions;

    public Adventurer(String name, String rating, double salaryMultiplier, Kingdom kingdom) {
        this.name = name;
        this.rating = rating;
        this.salaryMultiplier = salaryMultiplier;
        this.kingdom = kingdom;
        this.missions = new ArrayList<>();
    }
    public abstract int calculateSalary();
    public abstract int getMaxMissions();

    public void addMission(Mission mission) {
        missions.add(mission);
    }

    public String getKingdomName() {
        return kingdom.getName();
    }

    public String getMissionsString() {
        String misi = "";
        if(missions.size() <= 0){
        misi = "Tidak memiliki proyek";
        }
        else if (missions.size() == 1){
            misi += missions.get(0).getName();
        }
        else {
            for (int i = 0; i < missions.size(); i++){
            if (i == missions.size()-1){
            misi += missions.get(i).getName() + ",";
                }
                else {
            misi += missions.get(i).getName();
        }
            }
}
return misi;
    }

    @Override
    public String toString() {
        // TODO
        return null;
    }
    
    public String getName() {
        return name;
    }

}
